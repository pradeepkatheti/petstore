import mongoose from "mongoose";

const Sign_In = new mongoose.Schema({
    mail: {type: String,required: true,},
    fullname:{type: String,required: true},
    phonenumber:{type: String,required: true},
    gender:{type: String,required: true},
    phonenumber:{type: String,required: true},
    imageurl:{type: String,required: true},
    address:{type: String,required: true},
    dateofbirth: {type: String,required: true},
    company: {type: String,required: true},
    country: {type: String,required: true},
    state: {type: String,required: true},
    createpassword: {type:String,required:true},
    conformpassword: {type:String,required:true}

})

const Cart_Item = new mongoose.Schema({
    usermail: {type:String, required:true},
    productname : {type:String, required:true},
    price : {type:String, required:true},
    productimage : {type:String, required:true},
    quantity : {type:String, required:true},

})

const Pets_In_Store = new mongoose.Schema({
    image : {type:String, required:true},
    category : {type:String, required:true},
    description : {type:String, required:true},
    price: {type:String, required:true},
    title: {type:String, required:true},
    discount: {type:Number },

 

})

export {Sign_In, Cart_Item,Pets_In_Store}