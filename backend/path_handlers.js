import { SignIn_Data, Cart_Items , Pets_store} from "./model.js";
import { jwtGenator } from "./jwtTokengenaretor.js";
import nodemailer from "nodemailer";
import Stripe from "stripe";
import Razorpay from "razorpay";
import shortid from "shortid";
import { v4 as uuidv4 } from "uuid";
import { request, response } from "express";
//chaniging live or test keys here
const stripe = new Stripe(
  "sk_test_51LpvQPSFdnwA9qEOhvtXNfMWcsk7qWc3SQFzSH77WWUF37bJ93IbrGkd2SerxRF4MYwjX4rFdlSwssaAF3Zu1yOb00TCc8xsPx",
  {}
);

const razorpay = new Razorpay({
  key_id: "rzp_test_0vsANbC2ZGDZfD",
  key_secret: "UDi41SJ2RNRuWY8B3s6hFkLh",
});

// Register Handler

const Register_Handler = async (request, response) => {
  const {
    mail,
    fullname,
    phonenumber,
    imageurl,
    address,
    gender,
    state,
    country,
    company,
    dateofbirth,
    createpassword,
    conformpassword,
  } = request.body;

  const userData = {
    mail: mail,
    fullname: fullname,
    phonenumber: phonenumber,
    gender: gender,
    imageurl: imageurl,
    address: address,
    dateofbirth: dateofbirth,
    company: company,
    country: country,
    state: state,
    createpassword: createpassword,
    conformpassword: conformpassword,
  };

  SignIn_Data.findOne({ mail: mail }, (error, data) => {
    console.log(error, "error msg");
    if (data === null) {
      SignIn_Data.findOne({ phonenumber: phonenumber }, (error, data) => {
        console.log(error, "error msg");
        if (data === null) {
          const Userdata = new SignIn_Data(userData);
          Userdata.save().then(() =>
            response.send(
              JSON.stringify({
                status_code: 200,
                status_Massage: "DATA ADDED",
              })
            )
          );
        } else {
          response.send(
            JSON.stringify({
              status_code: 401,
              status_Massage: "Phone Number Already registered",
            })
          );
        }
      });
    } else {
      response.send(
        JSON.stringify({
          status_code: 401,
          status_Massage: "Email Already register please Login",
        })
      );
    }
  });
};

//User Login HAndler

const Login_user_handler = async (request, response) => {
  const { mail, password } = request.body;

  SignIn_Data.find({ mail: mail }, (error, data) => {
    try {
      if (data[0].conformpassword === password) {
        const jwt_token = jwtGenator(mail, password);
        console.log(password);
        response.send({ jwt_token: jwt_token, status_code: 200, data: data });
      } else {
        const obj = {
          status_code: 401,
          status_message: "invalid password",
        };
        response.send(obj);
      }
    } catch (error) {
      response.send({
        status_code: 401,
        status_message: "email not Found",
        error: error,
      });
    }
  });
};

// mail handler

const Sending_data_to_mail = (request, response) => {
  const email = request.body.mail;

  SignIn_Data.find({ mail: email }, (error, data) => {
    try {
      if (data[0].mail === email) {
        const transpoter = nodemailer.createTransport({
          host: "smtp.gmail.com",
          port: "587",
          service: "hotmail",
          auth: {
            user: "pkatheti@innominds.com",
            pass: "*$PS.9Ycpt",
          },
        });
        const options = {
          from: "pkatheti@innominds.com",
          to: email,
          subject: "Hi User Welcome to the Pradeep WebApplication here we GO:",
          text: `UserName : ${data[0].mail} & Your Password : ${data[0].conformpassword} `,
        };
        transpoter.sendMail(options, function (error, info) {
          if (error) {
            console.log(error);
            return;
          }
          console.log("sent", info.response);
        });
        response.send({
          status_msg: "Check your Email For Login Info ",
          status_code: 200,
        });
      }
    } catch (error) {
      response.send({ errormsg: "Email Not Found", status_code: 401 });
    }
  });
};

// getting data Handler

const Get_Userdata = (request, response) => {
  SignIn_Data.find({})
    .then((data) => {
      response.send(data);
    })
    .catch((err) => {
      console.log(err);
    });
};

//Payment Handler  with Stripe its not working in India

const Payment_Handler = (request, response) => {
  const { product, token } = request.body;
  const idempotencyKey = uuidv4();
  let status, error;
  let tokenid = token.id;

  try {
    (async () => {
      await stripe.charges.create({
        currency: "usd",
        amount: 10,
        source: token.id,
      });
      status = "Success";
      //   const customer = await stripe.customers.create({
      //     email: token.email,
      //     source: token.id
      //   }).then((data) => {
      //     console.log(data.id, "datat id coming");
      //     stripe.charges.create({
      //                 amount:product.price*100,
      //                 currency: "usd",
      //                 customer: data.id,
      //                 receipt_email: token.email,
      //                 description: product.name,
      //                 shipping: {
      //                   name: token.card.name,
      //                   address : {
      //                     country : token.card.address_country
      //                   }
      //                 }
      //               }, {idempotencyKey})
      //   });

      //   // console.log(customer.id);
    })();
  } catch (error) {
    console.log(error);
    status = "Failed";
  }

  response.send(status);
};

//Payment Handler with Razorpay

const Payment_Handler_Razorpay = async (request, res) => {
  const payment_capture = 1;
  const amount = request.body.amount;
  const currency = "INR";

  const options = {
    amount: amount * 100,
    currency,
    receipt: shortid.generate(),
    payment_capture,
  };

  try {
    const response = await razorpay.orders.create(options);
    console.log(response);
    res.json({
      id: response.id,
      currency: response.currency,
      amount: response.amount,
    });
  } catch (error) {
    console.log(error);
  }
};

const Adding_Cart_items = (request, response) => {
  const { productname, price, productimage, quantity, usermail } = request.body;
  const productDetails = {
    productname:productname,
    price:price,
    productimage:productimage,
    quantity:quantity,
    usermail:usermail
  }
  Cart_Items.find({productname:productname} , (error,data) => {
    console.log(error, "error msg");
  
    if(data.length ===  0 ) {
      const productdata = new Cart_Items(productDetails);
      productdata.save().then(() =>
        response.send(
          JSON.stringify({
            status_code: 200,
            status_Massage: "Product added to the cart. Click me to check",
          })
        )
      );
    }else{
      response.send(
        JSON.stringify({
          status_code: 403,
          status_Massage: "Product already in Cart. Click me to check",
        })
      )
    }
  })
 

};


const Getting_cart_data = (request,response) => {
  Cart_Items.find().then((data) => {
    response.send({data:data, status_Massage: "all data is here",
  status_code:200})
  }).catch((error) => {
    response.send({status_Massage:"no Cart Items", status_code: 400})
  })

}

const Delete_Cart_item = (request,response) => {
  const _id = request.body._id
  Cart_Items.deleteMany({_id:_id}).then((data) => {
    response.send({data:data,status_Massage:"CART item Removed", status_code:200})
  }).catch((error) => {
    response.send({status_Massage:"internal server error", status_code:400})
  })

}

//admin storing the pets details in data base


const Pets_Storing_Handler = async (request, response) => {
  const {
    image ,
    category,
    description ,
    price,
    title,
    discount,
  } = request.body;

  const petStoreData = {
    image:image ,
    category:category,
    description: description ,
    price : price,
    title : title,
    discount : discount,
  };

  const Petsdata = new Pets_store(petStoreData);
  Petsdata.save().then(() =>
    response.send(
      JSON.stringify({
        status_code: 200,
        status_Massage: "Pet Added",
        petDeatils: petStoreData
      })
    )
  );

 
};

const Getting_Pets_data = (request,response) => {
  Pets_store.find().then((data) => {
    response.send({data:data, status_Massage: "all data is here",
  status_code:200})
  }).catch((error) => {
    response.send({status_Massage:"no Pets in here", status_code: 400})
  })

}

export {
  Register_Handler,
  Get_Userdata,
  Login_user_handler,
  Sending_data_to_mail,
  Payment_Handler,
  Payment_Handler_Razorpay,
  Adding_Cart_items, 
  Getting_cart_data,
  Delete_Cart_item,
  Pets_Storing_Handler,
  Getting_Pets_data
};
