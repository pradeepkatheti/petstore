import mongoose from "mongoose";

import { Sign_In, Cart_Item,Pets_In_Store } from "./schema.js";

const SignIn_Data = mongoose.model("SignIn_Data", Sign_In);
const Cart_Items = mongoose.model("Cart_Items", Cart_Item);
const Pets_store = mongoose.model('Pets_store', Pets_In_Store)

export {SignIn_Data, Cart_Items,Pets_store}