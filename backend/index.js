import express from "express"
import cors from "cors"
import bodyParser from "body-parser"
import mongoose from "mongoose"
import { Forgot_user_handler } from "./nodemailer.js"
import {
    Register_Handler,
    Payment_Handler_Razorpay,
    Get_Userdata,
    Login_user_handler,
    Sending_data_to_mail,
    Payment_Handler,
    Adding_Cart_items,
    Getting_cart_data,
    Delete_Cart_item,
    Pets_Storing_Handler,
    Getting_Pets_data
} from "./path_handlers.js"

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors({ origin: "*" }));


// Api's 

app.post("/login", Login_user_handler)
app.post('/petstore', Pets_Storing_Handler)
app.post("/register", Register_Handler)
app.post("/forgot", Forgot_user_handler)
app.post('/razorpay', Payment_Handler_Razorpay)
app.post("/payment", Payment_Handler)
app.post("/addtocart", Adding_Cart_items)
app.get("/pets", Getting_Pets_data)
app.get("/getcartdata", Getting_cart_data)
app.get("/getdata", Get_Userdata)
app.post("/sendingotp", Sending_data_to_mail)
app.delete("/deletecartitem", Delete_Cart_item)



const port = "https://nameless-sea-91228.herokuapp.com"

mongoose.connect("mongodb+srv://pradeepkatheti:13255m052@cluster0.acy73.mongodb.net/?retryWrites=true&w=majority").then(() => {
    console.log("Data BaseConnected");
    app.listen(4000, () => {
        console.log("server is running at port 4000");
    })

})
    .catch(() => {
        console.log("Unable  to connect to database");
    })



