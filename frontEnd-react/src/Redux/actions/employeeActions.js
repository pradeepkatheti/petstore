import { actionTypes  } from "../contants/actionTypes";

export const setLogin = (loginData) => {
    return {
        type: actionTypes.SET_LOGIN_DATA,
        payload: loginData
    }
}

export const setCardItem = (caedItems) => {
    return {
        type:actionTypes.SET_CARD_ITEM,
        payload: caedItems
    }
}

export const setMailOtp = (otp) => {
    return {
        type:actionTypes.SET_MAILOPT,
        payload: otp
    }
}

export const setSelectedCartItem = (selecteditem) => {
    return {
        type: actionTypes.SET_SELECTED_CART_ITEM,
        payload: selecteditem
    }
}