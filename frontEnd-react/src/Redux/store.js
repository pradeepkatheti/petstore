
import {createStore} from "redux"
import reducers  from "./reducers";

import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
    key: "Newone",
    storage,
}


const persistingReducer = persistReducer(persistConfig, reducers);

const store = createStore(
  persistingReducer,
  {},
  window.__REDUX_DEVTOOLS_EXTENSION__()&& window.__REDUX_DEVTOOLS_EXTENSION__()
);
const persistor = persistStore(store);

export { persistor };
export default store;
