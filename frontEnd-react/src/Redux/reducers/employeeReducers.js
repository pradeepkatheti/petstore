import { actionTypes  } from "../contants/actionTypes";

const initialState = {
  
}

export const LoginReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case actionTypes.SET_LOGIN_DATA:
      return {...state, loginData:payload}
    default:
     return state
  }
}

export const CardItemReducer = (state= initialState, {type,payload}) => {
  switch (type) {
    case actionTypes.SET_CARD_ITEM:
      return {...state, cardItems: payload}
  
    default:
      return state
  }
}



export const mailOtpReducer = (state= initialState, {type,payload}) => {
  switch (type) {
    case actionTypes.SET_MAILOPT:
      return {...state, otp: payload}
  
    default:
      return state
  }
}

export const selectedcartitemReducer = (state=initialState, {type,payload}) =>{
  switch (type) {
    case actionTypes.SET_SELECTED_CART_ITEM :
      return {...state, selecteditem:payload}
  
    default:
      return state
  }
}