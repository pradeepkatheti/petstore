import {combineReducers} from "redux"
import { CardItemReducer, LoginReducer , mailOtpReducer, selectedcartitemReducer} from "./employeeReducers";

const reducers = combineReducers({
    siginData : LoginReducer,
    cardData: CardItemReducer,
    mailOtpData: mailOtpReducer,
    selectedItemdata: selectedcartitemReducer
})

export default reducers