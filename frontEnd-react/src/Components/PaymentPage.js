import { Button } from "@mui/material";
import React, { useState } from "react";
import StripeCheckout from "react-stripe-checkout"

function PaymentPage() {
  const [product, setProduct] = useState({
    name: "HOD Book",
    price: 10,
    productBy: "GOT"
  })
  const makePayment = async (token) => {
    const Obj = {
      product: product,
      token: token
    }

    const url = "http://localhost:4000/payment"
    const options = {
      method: "POST",
      body: JSON.stringify(Obj),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, options).then(response => {
      console.log(response, "response");
    }).catch(err => {
      console.log(err);
    })
  }
  return (
    <>
      <StripeCheckout stripeKey="pk_test_51LpvQPSFdnwA9qEO6P8FqOiqxgvaPisRKeNBs8CNxD1ZXNzmt4ZBPfAoSOauHdZo7bFu4XGtyrC3piFXcNI8ia7500t23H5NF0" token={makePayment} currency="USD" name="Buying HOD BOOK" amount={product.price}>
        <Button varient="contained">Pay Here</Button>
      </StripeCheckout>
    </>
  )
}


export default PaymentPage;


