
import * as React from "react";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import { Divider, TextField } from "@mui/material";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Link from "@mui/material/Link";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useSelector } from 'react-redux'
import { useState } from "react";
import { grey } from "@mui/material/colors";

import CardItems from "./CardItems";


function Copyright() {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://in.linkedin.com/in/pradeep-katheti-14420b207">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const theme = createTheme({
 
  typography: {
    fontFamily: "Quicksand",
    fontWeightBold:700,
    fontWeightMedium: 600,
    fontWeightRegular:500,
    fontWeightLight:400,
    
  },
  palette: {
    primary: {
      main: grey[400],
    },
    secondary: {
      main: grey[500],
    },
  },

});


export default function MainPage() {
  const info = useSelector((state) => state.siginData.loginData)
const [infoo, setInfoo] = useState([info.data[0]])
console.log(infoo[0], "pradeep");
 
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
   
      
      <main>

        <Box
          sx={{
            bgcolor: "#2d2d2d",
            pt: 8,
            pb: 6,
          }}
        >
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              gutterBottom
              color="primary"
            >
            { infoo[0].fullname}
            </Typography>
            <Typography
              variant="h5"
              align="center"
              color="secondary"
              paragraph
            >
              Welcome to Pet Store! As part of our prts club you’re among the first to access the only approved all-kind of prts for you. Stay tuned for exciting giveaways, VIP perks, and more!

🐣
            </Typography>
           

          </Container>

        </Box>

        
       
      </main>
      <CardItems/>
<Divider sx={{}}/>

      <Box sx={{ bgcolor: "#090A0A",  position:"static", p: 6 }} component="footer">
          
        <Typography variant="h6"  align="center" 
          color="primary"
          gutterBottom>
          Follow in Linkdin
        </Typography>
        <Typography
          variant="subtitle1"
          align="center"
          component="p"
          color="secondary"
        >
          Something here to give the footer a purpose!
        </Typography>
        <Copyright />
      </Box>
      {/* End footer */}
    </ThemeProvider>
  );
}


