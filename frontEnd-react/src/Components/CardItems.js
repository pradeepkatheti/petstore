import * as React from "react";
import axios from "axios";
import "./index.css";
import PaymentPageRzo from "./PaymentPageRzo";
import { setSelectedCartItem } from "../Redux/actions/employeeActions";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";
import {
  Button,
  Container,
  Dialog,
  DialogActions,
  DialogContentText,
  DialogContent,
  DialogTitle,
  useMediaQuery,
  useTheme,
  IconButton,
  Link,
  Typography,
  Card,
  Grid,
  Box,
  CardMedia,
  CardActionArea,
  CardContent,
  styled,
  createTheme,
  ThemeProvider,
  Divider,
} from "@mui/material";
import Draggable from "react-draggable";

import {Paper} from "@mui/material";
import { orange, grey } from "@mui/material/colors";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import HighlightOffRoundedIcon from "@mui/icons-material/HighlightOffRounded";

// Creating the Theme

const customTheme = createTheme({
  typography: {
    fontFamily: "Quicksand",
    fontWeightBold: 700,
    fontWeightMedium: 600,
    fontWeightRegular: 500,
    fontWeightLight: 400,
  },
  palette: {
    primary: {
      main: orange[900],
    },
    secondary: {
      main: grey[900],
    },
  },
});

// Styling the Box (Transitions) when hover
function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const StyledAvatar = styled(Box)`
  ${({ theme }) => `
  cursor: pointer;
  background-color: black;
  
  transition: ${theme.transitions.create(["background-color", "transform"], {
    duration: theme.transitions.duration.standard,
  })};
  &:hover {
    background-color: "black";
    transform: scale(1.1);
  
  }
  `}
`;

export default function CardItems() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [statusMsg, setStatusMsg] = useState("");
  const [quantity, setQuantity] = useState(1);
  const [data, setData] = useState([]);
  const [dataa, setDataa] = useState("");
  const [open, setOpen] = React.useState(false);

  dispatch(setSelectedCartItem(dataa));

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("xs"));
  const userInfo = useSelector((state) => state.siginData.loginData);
  const infoOfcarditem = useSelector(
    (state) => state.selectedItemdata.selecteditem
  );

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setStatusMsg("");
  };

  const AddingCartItem = async (props) => {
    const cartdetailes = {
      usermail: userInfo.data[0].mail,
      productname: infoOfcarditem.title,
      price: infoOfcarditem.price,
      productimage: infoOfcarditem.image,
      quantity: quantity,
    };
    const url = "http://localhost:4000/addtocart";
    const options = {
      method: "POST",
      body: JSON.stringify(cartdetailes),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, options);
    const data = await response.json();
    if (data.status_code === 200 || data.status_code === 403) {
      setStatusMsg(data.status_Massage);
    }
    console.log(data.status_Massage, "data");
  };

  useEffect(() => {
    axios.get("http://localhost:4000/pets").then((response) => {
      setData(response.data.data);
      console.log(response.data);
    });
  }, []);

  return (
    <ThemeProvider theme={customTheme}>
      {/* End hero unit */}
      <Grid container spacing={5} sx={{ py: 3, backgroundColor: "#2d2d2d" }}>
        {data.map((dataObj) => (
          <Grid
            item
            key={dataObj._id}
            xs={12}
            sm={6}
            md={3}
            sx={{ backgroundColor: "#2d2d2d" }}
          >
            <StyledAvatar
              // onMouseOver={() => {
              //   setDataa(dataObj);
              //   handleClickOpen();
              // }}

              onClick={() => {
                setDataa(dataObj);
                handleClickOpen();
              }}
            >
              <Card
                sx={{
                  overflowY: "auto",
                  backgroundColor: "#2d2d2d",
                  borderStyle: "solid",
                  borderWidth: 0.1,
                  height: 300,
                  color: "#948a8a",
                }}
              >
                <CardActionArea>
                  <CardMedia
                    component="img"
                    height="140"
                    image={dataObj.image}
                    alt="green iguana"
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {dataObj.category}
                    </Typography>
                    <Typography variant="body2" sx={{ color: "#948a8a" }}>
                      {dataObj.description}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </StyledAvatar>
          </Grid>
        ))}
      </Grid>

      <Dialog
        fullScreen={fullScreen}
        open={open}
        fullWidth="xs"
        maxWidth= 'sm'
        PaperComponent={PaperComponent}
        onClose={handleClose}
        aria-labelledby="draggable-dialog-title"
      >
        <Container
          sx={{ display: "flex", mt: 2, flexDirection: "row-reverse" }}
        >
          <HighlightOffRoundedIcon color="primary" onClick={handleClose} />
        </Container>
        <DialogTitle id="draggable-dialog-title">
          {"Product Detailes"}
        </DialogTitle>
        <Divider></Divider>
        <DialogContent>
          <CardMedia
            component="img"
            height="140"
            image={infoOfcarditem.image}
            alt="green iguana"
          />
          <DialogContentText
            variant="h5"
            sx={{ marginTop: 2 }}
            color="secondary"
          >
            {infoOfcarditem.title}
          </DialogContentText>
          <DialogContentText sx={{ marginTop: 2 }} color="secondary">
            Price: {infoOfcarditem.price} Rs
          </DialogContentText>
          <DialogContentText
            variant="h5"
            sx={{ marginTop: 2 }}
            color="secondary"
          >
            Rating:
          </DialogContentText>
          <DialogContent sx={{ display: "flex", flexDirection: "row" }}>
            <IconButton
              onClick={() => {
                if (quantity > 0) {
                  setQuantity(quantity - 1);
                }
              }}
            >
              <RemoveCircleOutlineIcon />
            </IconButton>
            <DialogContentText
              sx={{ marginRight: 2 }}
              color="secondary"
              variant="h4"
            >
              {quantity}
            </DialogContentText>
            <IconButton
              sx={{ marginRight: 2 }}
              onClick={() => {
                setQuantity(quantity + 1);
              }}
            >
              <AddCircleIcon />
            </IconButton>
          </DialogContent>
          <Link href="/cartpage">{statusMsg}</Link>
        </DialogContent>
        <DialogActions
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <PaymentPageRzo amount={infoOfcarditem.price} />
          <Button
            color="primary"
            variant="contained"
            onClick={AddingCartItem}
            autoFocus
          >
            Add to Cart
          </Button>
        </DialogActions>
      </Dialog>
    </ThemeProvider>
  );
}
