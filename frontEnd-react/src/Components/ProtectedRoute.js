import {Redirect, Route} from 'react-router-dom'
// import Login from '../Login'
import { useSelector } from 'react-redux'


const ProtectedRoute = props => {
  const info = useSelector((state) => state.siginData.loginData)
  console.log(info, "info");
  
  if (info.status_code !==200) {
    return <Redirect to="/singin" />
  }
  return <Route {...props} />
}

export default ProtectedRoute
