import * as React from "react";
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import Menu from "@mui/material/Menu";
import ListItemIcon from "@mui/material/ListItemIcon";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Tooltip from "@mui/material/Tooltip";
import Grid from "@mui/material/Grid";
import Stack from "@mui/material/Stack";
import MuiAppBar from "@mui/material/AppBar";
import PerfectScrollbar from 'react-perfect-scrollbar';
import Footer from "@mui/material/TableFooter"
import { ListItemButton, ListItemText} from "@mui/material";


import { useTheme } from "@mui/material/styles";
import { Link } from "@mui/material";

import "./index.css"



function NotificationAvatar() {
  const theme = useTheme();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <React.Fragment>
      <Box sx={{ display: "flex", alignItems: "start", textAlign: "center" }}>
        <Tooltip title="User Profile">
          <IconButton
            onClick={handleClick}
            size="small"
            sx={{ ml: 2 }}
            aria-controls={open ? "account-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
          >
            <Avatar
              alt="Remy Sharp"
              src="https://source.unsplash.com/random"
              sx={{ width: 51, height: 51 }}
            />
          </IconButton>
        </Tooltip>
      </Box>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
         <MuiAppBar position="static"  sx= {{backgroundColor : "white",alignItems: "center",color : "black"}}>
          <Grid item xs={12}>
            <Grid
              container
              alignItems="center"
              justifyContent="space-between"
              sx={{ pt: 2, px: 2 }}
            >
              <Grid item>
                <Stack direction="row" spacing={2}>
                  <Typography variant="subtitle1">User Notification</Typography>
                
                </Stack>
              </Grid>
              <Grid item>
                <Stack direction="row" spacing={2}>
                  <Typography variant="subtitle1">Mark</Typography>
                
                </Stack>
              </Grid>
             
            </Grid>
          </Grid>
        </MuiAppBar>
        
        

        <Grid item xs={12}>
            <PerfectScrollbar  style={{ height: '100%', maxHeight: 'calc(100vh - 205px)', overflowX:"auto"  }}>
            
            <ListItemButton href="Carditem" >
      <ListItemIcon>
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </ListItemButton>
    <ListItemButton href = "notification">
      <ListItemIcon>
      </ListItemIcon>
      <ListItemText primary="Orders" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
      </ListItemIcon>
      <ListItemText primary="Customers" />
    </ListItemButton>
    <ListItemButton href = "notification">
      <ListItemIcon>
      </ListItemIcon>
      <ListItemText primary="Orders" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
      <ListItemButton href = "notification">
      <ListItemIcon>
      </ListItemIcon>
      <ListItemText primary="Orders" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
      </ListItemIcon>
      <ListItemText primary="Customers" />
    </ListItemButton>
      </ListItemIcon>
      <ListItemText primary="Customers" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
      </ListItemIcon>
      <ListItemText primary="Reports" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
      </ListItemIcon>
      <ListItemText primary="Integrations" />
    </ListItemButton>
    
        </PerfectScrollbar>
        </Grid>
      <Divider />
      <Footer href = "carditems"  sx={{ height: 51, alignItems: 'center' }} >
        <Grid container display= 'flex' alignItems='center'>
          <Grid item flexDirection='row'>
        <Link textAlign='center'>Show All</Link>
        </Grid></Grid>
      </Footer>

      </Menu>
    </React.Fragment>
  );
}
export default NotificationAvatar;
