
import React, { useEffect, useRef } from 'react';
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import { createTheme, ThemeProvider } from "@mui/material/styles";


import "./index.css"



import VanillaTilt from 'vanilla-tilt';
import { Container } from '@mui/material';

function Tilt(props) {
  const { options, ...rest } = props;
  const tilt = useRef(null);

  useEffect(() => {
    VanillaTilt.init(tilt.current, options);
  }, [options]);

  return <div ref={tilt} {...rest} />;
}

export default  function HomeContent(){
 
  const options = {
    scale: 1.2,
    speed: 2000,
    max: 30
  };
  const mdTheme = createTheme();
  

    return (
        <ThemeProvider theme={mdTheme}>
        <Box sx={{ display: "flex" }}>
          <CssBaseline />
          <Container sx={{
                display: 'flex',
                flexDirection:'row',
                justifyContent:"center",
                alignItems:"center",
                flexGrow: 1,
                height:"85vh",
                width:"100vw",
              }}>
          <Tilt options={options} >

            <Box
             className="pp"
              component="main"
              sx={{
                  background: "linear-gradient(-45deg, #ee7752, #e73c7e, #23a6d5, #23d5ab)",
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems:'center',
                flexGrow: 1,
                height: "60vh",
                width: "60vw",

                overflow: "auto",
              }}
            >
        <div>
        <h1><span className="reveal">Any fool can write code that a COMPUTER can understand.</span></h1>
        <p>
        Good programmers write code that 
        </p><h1><span className="reveal"> HUMANS can understand.</span></h1>
        </div>
        </Box>
        </Tilt>
        </Container>
           
          </Box>
        </ThemeProvider>
    )
}