import React, { useState } from "react";
import { Button } from "@mui/material";
import { Fab } from "@mui/material";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import PaidIcon from "@mui/icons-material/Paid";
import { createTheme, ThemeProvider } from "@mui/material";
import { orange } from "@mui/material/colors";

import IconButton from "@mui/material/IconButton";
const theme = createTheme({
 
  typography: {
    fontFamily: "Quicksand",
    fontWeightBold:700,
    fontWeightMedium: 600,
    fontWeightRegular:500,
    fontWeightLight:400,
    
  },
  palette: {
    primary: {
      main: orange[900],
    }
    
  },

});

function loadScript(src) {
  return new Promise((resolve) => {
    const script = document.createElement("script");
    script.src = src;
    script.onload = () => {
      resolve(true);
    };
    script.onerror = () => {
      resolve(false);
    };
    document.body.appendChild(script);
  });
}

const __DEV__ = document.domain === "localhost";

function PaymentPageRzo(props) {
  const [name, setName] = useState(10);
  const {amount} = props
  const displayRazorpay = async (event) => {
    event.preventDefault();

    const res = await loadScript(
      "https://checkout.razorpay.com/v1/checkout.js"
    );

    if (!res) {
      alert("Razorpay SDK failed to load. Are you online?");
      return;
    }
    const Obj = {
      amount: amount,
    };

    const options = {
      method: "POST",
      body: JSON.stringify(Obj),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const data = await fetch("http://localhost:4000/razorpay", options).then(
      (t) => t.json()
    );

    console.log(data);
//changing test and live keys here
    const asdds = {
      key: __DEV__ ? "rzp_test_0vsANbC2ZGDZfD" : "PRODUCTION_KEY",
      currency: data.currency,
      amount: data.amount.toString(),
      order_id: data.id,
      name: "Buy",
      description: "Thank you for Buying our Products. Please visit Again",
      image:
        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg",
      handler: function (response) {
        alert(response.razorpay_payment_id);
        alert(response.razorpay_order_id, "pradepep");
        alert(response.razorpay_signature);
      },
      prefill: {
        name,
        email: "pradeepkatheti@gmail.com",
        phone_number: "994836134",
      },
    };
    const paymentObject = new window.Razorpay(asdds);
    paymentObject.open();
  };

  return (
    <ThemeProvider theme={theme}>
    <Button
    variant="contained"
      color="primary"
      onClick={displayRazorpay}
      aria-label="add to shopping cart"
    >
     Buy Now
    </Button></ThemeProvider>
  );
}

export default PaymentPageRzo;
