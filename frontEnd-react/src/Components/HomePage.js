import * as React from "react";
import { styled, ThemeProvider } from "@mui/material/styles";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Badge from "@mui/material/Badge";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import { mainListItems, secondaryListItems } from "./ListItems";
import NotificationAvatar from "./Notification";
import Button from "@mui/material/Button";
import { setLogin } from "../Redux/actions/employeeActions";
import { useDispatch } from "react-redux";
import { useMediaQuery,useTheme } from "@mui/material";



const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  "& .MuiDrawer-paper": {
    position: "relative",
    whiteSpace: "nowrap",
    backgroundColor: "#151517",
    color: "#635a5a",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxSizing: "border-box",
    ...(!open && {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9),
      },
    }),
  },
}));

function HomePages() {
  const [open, setOpen] = React.useState(false);

  const theme = useTheme();
  
  const dispatch = useDispatch()
  console.log(open);


  const toggleDrawer = () => {
    setOpen(!open);
  };
  const  handlework = () =>  {
    alert('Thanks')
    dispatch(setLogin(["user Logout"]))
  };

  return (
    <ThemeProvider theme={theme}>
      <AppBar  open={open} 
    >
        <Toolbar
          sx={{
            pr: "24px",
            background: "#090A0A",
            // keep right padding when drawer closed
          }}
        >
          <IconButton
            edge="start"
            color="inherit"
            aria-label="close drawer"
            onClick={toggleDrawer}
            sx={{
              marginRight: "36px",
              ...(open && { display: "none" }),
            }}
          >
            <MenuIcon sx={{color: "#635a5a"}}/>
          </IconButton>
          <Typography
            component="h1"
            variant="h6"
            color="#635a5a"
            noWrap
            sx={{ flexGrow: 1 }}
          >
            User Name
          </Typography>
          <Button onClick={handlework}>logout</Button>
         
            <Badge badgeContent={2} color="success" sx={{mt:1, ml:2,mr: 2 , mb:1}}>
              <NotificationAvatar />
            </Badge>
          

        </Toolbar>
      </AppBar>
      <Drawer variant="permanent"open={open} >
        <Toolbar
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
            px: [1],
          }}
        >
          <IconButton onClick={toggleDrawer}>
            <ChevronLeftIcon sx={{ color: "#635a5a" }} />
          </IconButton>
        </Toolbar>
        <Divider sx={{ color: "#635a5a" }} />
        <List component="nav">
          {mainListItems}
          <Divider sx={{ my: 1, color: "#635a5a" }} />
          {secondaryListItems}
        </List>
      </Drawer>
    </ThemeProvider>
  );
}

export default function HomePage() {
  return <HomePages />;
}
