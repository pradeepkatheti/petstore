import React, { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import ButtonBase from "@mui/material/ButtonBase";
import axios from "axios";
import PaymentPageRzo from "./PaymentPageRzo";
import { Container, Button } from "@mui/material";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import "./cartpage.css"
import { IconButton } from "@mui/material";
const Img = styled("img")({
  margin: "auto",
  display: "block",
  maxWidth: "100%",
  maxHeight: "100%",
});

function CartPage(props) {
  const [price, setPrice] = useState(0)
  const [data, setData] = useState();
  console.log(data, "not working");


  const RemoveCartItem = async (index, item) => {
    const Obj = {
      _id: item._id,
    };
    console.log(Obj);
    const url = "http://localhost:4000/deletecartitem";
    const options = {
      method: "DELETE",
      body: JSON.stringify(Obj),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const responsedata = await fetch(url, options);
    const data = await responsedata.json();
    console.log(data, "pradeep");
  };
  
 
   

  

  const addQuantity = (cart_id) => {
    setData(
      data.map((item) =>
        item._id === cart_id
          ? { ...item, quantity: Number(item.quantity) + 1 }
          : item
      )
    );
  };
  const removeQuantity = (cart_id) => {
    setData((data) =>
      data.map((item) =>
      
        item._id === cart_id & item.quantity > 0
          ? { ...item, quantity:  Number(item.quantity) - 1 }
          : item
      )
    );
  };
  let sum = 0
  const totaprice = () => {
    for (let i of data) {
      sum = sum+(Number(i.price)*Number(i.quantity))

    }
    setPrice(sum)
    console.log(sum);
  }
  useEffect(() => {
    axios.get("http://localhost:4000/getcartdata").then((response) => {
      console.log(response.data.data, "pradeep its working");
      setData(response.data.data);
    });
  }, []);

 
  return (
    <Container>
      <p>Cart Page</p>

      <>
        {data?.map((item, index) => (
          <Paper
            onClick={() => {
              console.log(item);
            }}
            key={item._id}
            sx={{
              p: 2,
              marginTop: 2,
              maxWidth: "100%",
              flexGrow: 1,
              backgroundColor: "#1A2027",
            }}
          >
            <Grid container spacing={6}>
              <Grid item>
                <ButtonBase sx={{ width: 128, height: 128 }}>
                <div className="container">
                    <div className="box">
                        <div className="imgBox">
                            <img src={item.productimage} alt="" />
                        </div>
                
                <div className="content">
                    <h5 className="heading">
                       {item.productname}
                    </h5>
                </div>
                </div>
                </div>
                  {/* <Img alt="complex" src={item.productimage} /> */}
                </ButtonBase>
             
              </Grid>
             
              <Grid item xs={12} sm container>
                <Grid item xs container direction="column" spacing={2}>
                  <Grid item xs>
                    <Typography
                      color={"white"}
                      gutterBottom
                      variant="subtitle1"
                      component="div"
                    >
                      Product Name : {item.productname}
                    </Typography>
                    <Grid sx={{ display: "flex", flexDirection: "row" }}>
                      <IconButton
                        onClick={() => {
                          removeQuantity(item._id);
                        }}
                      >
                        <RemoveCircleOutlineIcon />
                      </IconButton>
                      <Typography
                        sx={{ marginRight: 2 }}
                        color="secondary"
                        variant="h4"
                      >
                        {item.quantity}
                      </Typography>
                      <IconButton
                        sx={{ marginRight: 2 }}
                        onClick={() => {
                          addQuantity(item._id);
                        }}
                      >
                        <AddCircleIcon />
                      </IconButton>
                    </Grid>
                    <Typography variant="body2">ID: 1030114</Typography>
                  </Grid>
                  <Grid item>
                    <Button
                      sx={{ cursor: "pointer", color: "#ff5722" }}
                      variant="body2"
                      onClick={() => {
                        RemoveCartItem(index, item);
                      }}
                    >
                      Remove
                    </Button>
                  </Grid>
                </Grid>
                <Grid item>
                  <Typography
                    color={"white"}
                    variant="subtitle1"
                    component="div"
                  >
                    price: ${item.quantity * item.price}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        ))}
      </>
     
     <Typography>Total Price : Rs {price}</Typography>
     <Button onClick={totaprice}>

     <PaymentPageRzo onClick={totaprice} amount={price} sx={{ marginTop: 4 }} />

     </Button>
    </Container>
  );
}

export default CartPage;
 