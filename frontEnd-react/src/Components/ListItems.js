import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import BarChartIcon from '@mui/icons-material/BarChart';
import LayersIcon from '@mui/icons-material/Layers';
import AssignmentIcon from '@mui/icons-material/Assignment';
import HomeIcon from '@mui/icons-material/Home';
import ShoppingBasketIcon from '@mui/icons-material/ShoppingBasket';

export const mainListItems = (
  <React.Fragment>
    <ListItemButton href="Carditem" >
      <ListItemIcon>
        <HomeIcon sx = {{color:"#635a5a"}} />
      </ListItemIcon>
      <ListItemText primary="Home Page" />
    </ListItemButton>
    <ListItemButton href = "card">
      <ListItemIcon>
        <ShoppingBasketIcon  sx = {{color:"#635a5a"}}/>
      </ListItemIcon>
      <ListItemText primary="Products" />
    </ListItemButton>
    <ListItemButton href = "cartpage">
      <ListItemIcon>
      <ShoppingCartIcon  sx = {{color:"#635a5a"}}/>
      </ListItemIcon>
      <ListItemText primary="Cart Page" />
    </ListItemButton>
    <ListItemButton href = "reports">
      <ListItemIcon>
      <LayersIcon  sx = {{color:"#635a5a"}}/>
      </ListItemIcon>
      <ListItemText primary="Reports" />
    </ListItemButton>
    <ListItemButton href = "dashboard">
      <ListItemIcon>
      <BarChartIcon  sx = {{color:"#635a5a"}}/>
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </ListItemButton>
  </React.Fragment>
);

export const secondaryListItems = (
  <React.Fragment>
    <ListSubheader component="div" inset>
      Saved reports
    </ListSubheader>
    <ListItemButton>
      <ListItemIcon>
        <AssignmentIcon sx = {{color:"#635a5a"}} />
      </ListItemIcon>
      <ListItemText primary="Current month" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
        <AssignmentIcon sx = {{color:"#635a5a"}}/>
      </ListItemIcon>
      <ListItemText primary="Last quarter" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
        <AssignmentIcon sx = {{color:"#635a5a"}} />
      </ListItemIcon>
      <ListItemText primary="Year-end sale" />
    </ListItemButton>
  </React.Fragment>
);