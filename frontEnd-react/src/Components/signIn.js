import * as React from "react";
import { useState } from "react";
import {
  Avatar,
  Button,
  CssBaseline,
  TextField,
  Grid,
  Box,
  Typography,
  Container,
  createTheme,
  ThemeProvider,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { useHistory } from "react-router-dom";
import { setLogin } from "../Redux/actions/employeeActions";
import { useDispatch } from "react-redux";
import ForgotPwd from "./ForgotPwd";
import ResponsiveDialog from "./RegisterForm";
const theme = createTheme({
  typography: {
    fontFamily: "Quicksand",
    fontWeightBold: 700,
    fontWeightMedium: 600,
    fontWeightRegular: 500,
    fontWeightLight: 400,
  },
});

export default function SignIn() {

  localStorage.setItem("islogin", false);
  const [error, setError] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();
  
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const Obj = {
      mail: data.get("email"),
      password: data.get("password"),
    };
    console.log(Obj, "asdfsdf");

    if (Obj.mail === "" || Obj.password === "") {
      setError("Please enter the fields");
    } else {
      const url = "http://localhost:4000/login";
      const options = {
        method: "POST",
        body: JSON.stringify(Obj),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      const response = await fetch(url, options);
      const jwtData = await response.json();
      console.log(jwtData, "akjsdfasjdfa");
      if (jwtData.status_code === 200) {
        localStorage.setItem("islogin", true);
        dispatch(setLogin(jwtData));

        history.push("/");
        window.location.reload(true);
      } else {
        setError(
          "Opps! Something went wrong. Please enter the Valid Email & Password"
        );
      }
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box
            component="form"
            onSubmit={handleSubmit}
            noValidate
            sx={{ mt: 1 }}
          >
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus={true}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Typography sx={{ color: "red", textAlign: "center" }}>
              {error}
            </Typography>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign In
            </Button>
            <Grid
              container
              sx={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Grid item xs>
                <ForgotPwd />
              </Grid>
              <Grid item xs>
                <ResponsiveDialog />
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
