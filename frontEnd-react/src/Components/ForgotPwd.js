import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import HighlightOffRoundedIcon from "@mui/icons-material/HighlightOffRounded";
import { Container, textAlign } from "@mui/system";
import { Slide, TextField, Box, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { setMailOtp } from "../Redux/actions/employeeActions";
import { useHistory } from 'react-router-dom';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function ForgotPwd(props) {
  const history = useHistory()
  const dispatch = useDispatch()
  const info = useSelector((state) => state.mailOtpData.otp)
  console.log(info, "info")
  const [open, setOpen] = React.useState(false);
  const [mail, setMail] = React.useState("");
  const [ottp, setOttp] = React.useState("");
  const [error, setError] = React.useState("")
  const [sucessmsg, setSucessmsg] = React.useState("")
  const [disabledd, setDisabledd] = React.useState(true)
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("xs"));
  
  const handleClickOpen = async () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const hiddenfn = async () => {
    const Obj = {
      mail: mail

    }
    console.log(Obj.mail, "asdfsdf");

    if (Obj.mail === "") {
      setError("Please enter the fields")
    } else {
      const url = "http://localhost:4000/forgot"
      const options = {
        method: "POST",
        body: JSON.stringify(Obj),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      const response = await fetch(url, options)
      const Data = await response.json()
      dispatch(setMailOtp(Data))
      if (Data.status_code !== 200) {
        setError(Data.errormsg)
        setMail("")

      } else {
        setDisabledd(false)
      }
    }
  }


  const submitingOtp = async () => {
    if (info.otp === Number(ottp)) {
      const Obj = {
        mail: info.mail

      }

      if (Obj.mail === "") {
        setError("Please enter the fields")
      } else {
        const url = "http://localhost:4000/sendingotp"
        const options = {
          method: "POST",
          body: JSON.stringify(Obj),
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        };
        const response = await fetch(url, options)
        const pata = await response.json()
        console.log(pata, "osadpofasd");


        if (pata.status_code === 200) {
          setSucessmsg(pata.status_msg)
          handleClose()
          dispatch(setMailOtp([]))
        }
      }



    } else {
      setError("Inavalid OTP")
    }
  }

  return (
    <div>

      <Button variant="text" sx={{ textTransform: 'lowercase' }} onClick={handleClickOpen}>
        Forgot Password  ?
      </Button>
      <Dialog fullScreen={fullScreen}
        TransitionComponent={Transition}
        open={open} onClose={handleClose}>
        <Container
          sx={{ display: "flex", mt: 2, flexDirection: "row-reverse" }}
        >
          <HighlightOffRoundedIcon color="primary" onClick={handleClose} />
        </Container>

        <DialogContent>
          <Container>
            <Box sx={{
              display: "flex",
              flexDirection: 'column',
              alignItems: 'center',
            }}>
              <TextField margin="normal" autoFocus="true" onChange={(event) => { setMail(event.target.value) }} label="Email" required placeholder="Verify your Email"></TextField>
              <Typography sx={{ color: "red" }}>{error}</Typography>
              <Button variant="text" sx={{ textTransform: 'uppercase' }} onClick={hiddenfn} >
                Verify
              </Button>
              <TextField margin="normal" onChange={(event) => { setOttp(event.target.value) }} disabled={disabledd} label="OTP" required placeholder="Please enter your OTP"></TextField>
              <Button variant="contained" disabled={disabledd} sx={{ textTransform: 'uppercase', }} onClick={submitingOtp} >
                Submit OTP
              </Button>
              <Typography sx={{ color: "green" }}>{sucessmsg}</Typography>
              <Typography sx={{ color: "brown",mt :2  }} disabled="true">NOTE* : Check Your Email For Login Info</Typography>

            </Box>
          </Container>
        </DialogContent>
      </Dialog>
    </div>
  );
}
