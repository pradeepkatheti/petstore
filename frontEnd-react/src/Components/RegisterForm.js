import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import Form from "./Form";
import HighlightOffRoundedIcon from "@mui/icons-material/HighlightOffRounded";
import { Container } from "@mui/system";
import { Slide } from "@mui/material";


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function ResponsiveDialog(props) {
  const [open, setOpen] = React.useState(false);
  const [countries, setCountries] = React.useState(null)
  const[states, setStates] = React.useState(null)
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("xs"));

  const handleClickOpen = async() => {
   
      const url = "https://pkgstore.datahub.io/core/world-cities/world-cities_json/data/5b3dd46ad10990bca47b04b4739a02ba/world-cities_json.json";
      const options = {      
                       
              method: 'GET',    
              }
      const response = await fetch(url,options);
      const data = await response.json();
      setStates(data)

      let countrydata = []
      data.map((item) => {
        countrydata.push(item.country)
      })
    setCountries(new Set(countrydata))
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="text" sx={{ textTransform: 'lowercase' }} onClick={handleClickOpen}>
        Click here to Registration
              </Button>

      <Dialog fullScreen={fullScreen} 
        TransitionComponent={Transition}
         open={open} onClose={handleClose}>
        <Container
          sx={{ display: "flex", mt: 2, flexDirection: "row-reverse" }}
        >
          <HighlightOffRoundedIcon color="primary" onClick={handleClose} />
        </Container>

        <DialogContent>
          <Form info={countries} states = {states}/>
        </DialogContent>
      </Dialog>
    </div>
  );
}
