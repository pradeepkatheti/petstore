import React, { useEffect, useState } from "react";
import { Radio } from "antd";
import { Input } from "antd";
import "antd/dist/antd.css";
import MuiPhoneNumber from "material-ui-phone-number";
import AppRegistrationIcon from "@mui/icons-material/AppRegistration";
import Icon from "@ant-design/icons";
import { CheckCircleFilled, FileImageFilled } from "@ant-design/icons";
import { useHistory } from 'react-router-dom';



import CircularProgress from '@mui/material/CircularProgress';
import { green } from '@mui/material/colors';




import {
  MenuItem,
  Select,
  InputLabel,
  Dialog,
  DialogContentText,
  DialogContent,
  createTheme,
  ThemeProvider,
  Typography,
  Container,
  Fab,
  Box,
  Avatar,
  TextField,
  CssBaseline,
} from "@mui/material";
import { Upload } from "upload-js";












const { TextArea } = Input;

const options = [
  {
    label: "Male",
    value: "Male",
  },
  {
    label: "Female",
    value: "Female",
  },
  {
    label: "Others",
    value: "Others",
  },
];

const theme = createTheme({
  typography: {
    fontFamily: "Quicksand",
    fontWeightBold: 700,
    fontWeightMedium: 600,
    fontWeightRegular: 500,
    fontWeightLight: 400,
  },
});

export default function Form(props) {
  const history = useHistory()
  const { info, states } = props;
  let ppp = [];

  const timer = React.useRef();
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [age, setAge] = React.useState("");
  const [statee, setStatee] = React.useState("");
  const [liststates, setListstates] = React.useState([]);
  const [value3, setValue3] = useState("");
  const [imageurl, setImageurl] = useState("");
  const [address, setAddress] = useState("");
  const [errormsg, setErrormsg] = useState("");
  const [dateofbirth, setDateofbirth] = useState("");
  const [company, setCompany] = useState("");
  const [conformpassword, setConformpassword] = useState("");
  const [open, setOpen] = React.useState(false);

  const countriesData = Array.from(info).sort();
  let currentCountry = countriesData[age];
  let currentState = liststates[statee];

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const handledata = (event) => {
    setStatee(event.target.value);
  };

  const onChange3 = ({ target: { value } }) => {
    setValue3(value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const getState = () => {
    for (let i of states) {
      if (currentCountry === i.country) ppp.push(i.subcountry);
    }
    let lll = new Set(ppp);
    setListstates(Array.from(lll).sort());

  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const usesrData = {
      mail: data.get("email"),
      fullname: data.get("fullname"),
      phonenumber: data.get("phonenumber"),
      createpassword: data.get("createpassword"),
      conformpassword: conformpassword,
      dateofbirth: dateofbirth,
      company: company,
      country: currentCountry,
      state: currentState,
      gender: value3,
      address: address,
      imageurl: imageurl,
    };
    if (
      usesrData.address === "" ||
      usesrData.fullname === "" ||
      usesrData.phonenumber === "" ||
      usesrData.imageurl === "" ||
      usesrData.mail === "" ||
      usesrData.gender === "" ||
      usesrData.company === "" ||
      usesrData.country === "" ||
      usesrData.state === undefined ||
      usesrData.dateofbirth === "" ||
      usesrData.conformpassword === "" ||
      usesrData.createpassword === ""
    ) {
      setErrormsg(
        "Opps! Something went wrong. Your missed one of the text fields. (OR)  the given Email already taken"
      );
    } else if (usesrData.conformpassword !== usesrData.createpassword) {
      setErrormsg("Opps! Something went wrong. ");
    } else {
      const url = "http://localhost:4000/register";
      const options = {
        method: "POST",
        body: JSON.stringify(usesrData),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };

      const response = await fetch(url, options);
      const dataa = await response.json();
      console.log("data added", dataa);

      if (dataa.status_code === 200) {
        setOpen(true);
        history.push("/singin")

      } else {
        setErrormsg(dataa.status_Massage);

        // return <Redirect to="/" />;
      }
    }

    console.log("usesrDAta", usesrData);
  };

  useEffect(() => {
    getState();

  }, [age]);
  React.useEffect(() => {
    return () => {
      clearTimeout(timer.current);
    };
  }, []);

  const upload = new Upload({ apiKey: "free" });

  async function onFileSelected(event) {
    if (event.target.value !== "") {
      if (!loading) {
        setSuccess(false);
        setLoading(true);
        timer.current = window.setTimeout(() => {
          setSuccess(true);
          setLoading(false);
        }, 6600);
      }
    }
    const [file] = await event.target.files;
    const { fileUrl } = await upload.uploadFile(file);
    console.log(`File uploaded: ${fileUrl}`);
    setImageurl(fileUrl);
  }

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 2,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          {" "}
          <Avatar sx={{ m: 1, bgcolor: "primary.main", height: 50, width: 50 }}>
            <AppRegistrationIcon />
          </Avatar>
          <Typography variant="h5" sx={{ color: "#635a5a" }}>
            Register Here
          </Typography>
          <Box
            component="form"
            onSubmit={handleSubmit}
            noValidate
            sx={{ mt: 1 }}
          >
            <TextField
              margin="normal"
              required
              fullWidth
              id="fullname"
              label="Full Name"
              name="fullname"
              autoComplete="fullname"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="createpassword"
              label="Create Password"
              type="password"
              id="createpassword"
              autoComplete="current-password"
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="conformPassword"
              label="Conform Password"
              type="password"
              id="conformpassword"
              onChange={(e) => {
                setConformpassword(e.target.value);
              }}
              autoComplete="current-password"
            />
            <TextField
              id="date"
              label="Date of Birth"
              type="date"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={(e) => {
                setDateofbirth(e.target.value);
              }}
              margin="normal"
              fullWidth
              required
            />

            <MuiPhoneNumber
              margin="normal"
              required
              fullWidth
              id="phonenumber"
              name="phonenumber"
              autoComplete="phonenumber"
              preferredCountries={["in"]}
              disableAreaCodes={true}
              defaultCountry={"in"}
              variant="outlined"
              type="tel"
              label="Mobile Number"
            />
            <InputLabel id="demo-simple-select-label">Country</InputLabel>
            <Select
              margin="normal"
              fullWidth
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={age}
              label="i"
              sx={{ mt: 1 }}
              onChange={handleChange}
            >
              {countriesData.map((item) => {
                return (
                  <MenuItem value={countriesData.indexOf(item)}>
                    {item}
                  </MenuItem>
                );
              })}
            </Select>

            <InputLabel id="demos-simple-select-label">State</InputLabel>
            <Select
              margin="normal"
              fullWidth
              labelId="demos-simple-select-label"
              id="demos-simple-select"
              label="i"
              value={statee}
              sx={{ mt: 1 }}
              onChange={handledata}
            >
              {liststates?.map((item) => {
                return (
                  <MenuItem value={liststates.indexOf(item)}>{item}</MenuItem>
                );
              })}
            </Select>
            <Typography sx={{ color: "gray", mb: 1 }}>Gender</Typography>
            <Radio.Group
              options={options}
              onChange={onChange3}
              value={value3}
              optionType="button"
              buttonStyle="solid"
            />

            <TextField
              id="company"
              label="Current Working Company"
              type="text"
              margin="normal"
              fullWidth
              onChange={(e) => {
                setCompany(e.target.value);
              }}
              required
            />
            <Typography sx={{ color: "gray", mb: 1 }}>Address</Typography>
            <TextArea
              required
              id="address"
              onChange={(e) => {
                setAddress(e.target.value);
              }}
              autoComplete="address"
              placeholder="Enter your Address"
              rows={4}
            />

            <Typography sx={{ color: "gray", mb: 1 }}>
              Upload Your Image
            </Typography>

            <>
              <Input
                margin="normal"
                required
                addonBefore="Image"
                allowClear="true"
                type="file"
                variant="contained"
                prefix={
                  <FileImageFilled
                    style={{ color: "skyblue", fontSize: "20px" }}
                  />}
                disabled={loading}
                onChange={(event) => {
                  onFileSelected(event);
                }}
              />
              {loading && (
                <CircularProgress
                  size={53}
                  sx={{
                    color: green[500],
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    marginTop: '-12px',
                    marginLeft: '-12px',
                  }}
                />
              )}
            </>


            <Container
              sx={{
                mt: 2,
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
              }}
            >
              <Typography sx={{ color: "red", mb: 1 }}>{errormsg}</Typography>
            </Container>
            <Container
              sx={{
                mt: 2,
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
              }}
            >
              <Fab
                type="submit"
                color="primary"
                variant="extended"
                sx={{ width: "40%" }}
              >
                Register
              </Fab>
            </Container>
          </Box>
        </Box>
        <Dialog
          open={open}
          keepMounted
          onClose={handleClose}
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogContent>
            <Container
              sx={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
              }}
            >
              <Icon
                component={CheckCircleFilled}
                style={{
                  fontSize: "100px",
                  color: "green",
                }}
              />
            </Container>
            <DialogContentText
              sx={{ fontSize: 20 }}
              id="alert-dialog-slide-description"
            >
              Thank's for Your Registration
            </DialogContentText>
          </DialogContent>
        </Dialog>
      </Container>
    </ThemeProvider>
  );
}






