import "./App.css";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import CardItem from "./Components/CardItems";
import HomePage from "./Components/HomePage";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import HomeContent from "./Components/HomeContent";
import MainPage from "./Components/MainPage";
import CartPage from "./Components/CartPage";
import { useState } from "react";
import { useSelector } from "react-redux";
import Dashboard from "./Components/Dashboard";
import SignIn from "./Components/signIn";
import ProtectedRoute from "./Components/ProtectedRoute";
import Reports from "./Components/Reports";
const theme = createTheme({
  typography: {
    fontFamily: "Quicksand",
    fontWeightBold: 700,
    fontWeightMedium: 600,
    fontWeightRegular: 500,
    fontWeightLight: 400,
  },
});

function App() {
  const [infoo, setInfoo] = useState(
    useSelector((state) => state.siginData.loginData)
  );
  // console.log(infoo, "pradeep");
  const islogin = JSON.parse(localStorage.getItem("islogin"));

  const renderingPage = (infooo) => {
    if (islogin) {
      if (infooo[0] === "user Logout") {
        console.log("Usre Logout");
      } else {
        if (infooo.data[0].mail === "pradeepkatheti@gmail.com") {
          return (
            <>
              <ThemeProvider theme={theme}>
                <Box sx={{ display: "flex" }}>
                  <CssBaseline />
                  <HomePage />

                  <Box
                    component="main"
                    sx={{
                      backgroundColor: "#2d2d2d",
                      flexGrow: 1,
                      height: "100vh",
                      overflow: "auto",
                      mt: 9.6,
                    }}
                  >
                    <ProtectedRoute exact path="/" component={HomeContent} />
                    <ProtectedRoute
                      exact
                      path="/Carditem"
                      component={MainPage}
                    />
                    <ProtectedRoute exact path="/card" component={CardItem} />
                    <ProtectedRoute
                      exact
                      path="/cartpage"
                      component={CartPage}
                    />
                    <ProtectedRoute exact path="/reports" component={Reports} />
                    <ProtectedRoute
                      exact
                      path="/dashboard"
                      component={Dashboard}
                    />
                  </Box>
                </Box>
              </ThemeProvider>
            </>
          );
        } else {
          return (
            <>
              <ProtectedRoute exact path="/" component={HomePage} />
            </>
          );
        }
      }
    }
  };
  const pageData = renderingPage(infoo);

  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route exact path="/singin" component={SignIn} />
          {pageData}
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
